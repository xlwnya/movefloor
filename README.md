# MovingFloor sample for VRChat
- UdonSharpで作ったいわゆるファイナルしない床のサンプルです。
- This is a sample of a floor that moves the player in the same direction as the movement of the floor.

- Sample World: https://vrchat.com/home/launch?worldId=wrld_c9d8fe58-b5bc-4975-8777-45ce47db9593

## Requirement
- VRCSDK3-WORLD (tested with 2021.07.12.18.53_Public)
- UdonSharp https://github.com/MerlinVR/UdonSharp (tested with v0.20.0)
- VRC_StarterKit SDK3 https://gitlab.com/xlwnya/vrc_starterkit/-/releases (tested with 20210615)

## etc
- The player can respawn on moving floor. / 床の上でリスポーンしても動作に問題ないことを確認しました。
- The player can use Holoport(3P Locomotion). / Holoport(3P Locomotion)移動でも使用可能なことを確認しました。(床から歩いて脱出できる位置に床がある必要があります)
- The player can use Station. / 椅子対応を行いました(一定時間床の上からいなくなった場合処理をOFFにします)

## ChangeLog
- 2021-07-21
  - SetVelocity版を削除
  - Remove SetVelocity version.
- 2021-07-20
  - TeleportTo方式の床を追加 / Add TeleportTo version.
- 2021-06-17
  - デバッグ用Owner変更機能を使用した場合に床位置の同期が動かなくなる問題を修正。
  - Fix moving floor not syncing.
- 2021-06-15
  - Update/LateUpdate/FixedUpdate でそれぞれPlayerMoverを作成
  - 椅子対応
  - make PlayerMover for Update/LateUpdate/FixedUpdate.
  - Support chair.
- 2021-06-02
  - 床の同期方式を変更しました。(常に同期されるように変更されました)
  - Animate Physics / FixedUpdate版(MFloorPhysics.cs & MFloorPhysicsPlayerMover.cs)を追加しました(より安定している気がします)
  - デバッグ用の処理を追加
  - Change moving floor sync method. The floor now synced continuously.
  - Add Animate Physics / FixedUpdate version. (Maybe more stable.)
  - Add some debug function.

## License

These codes are licensed under CC0.

[![CC0](http://i.creativecommons.org/p/zero/1.0/88x31.png "CC0")](http://creativecommons.org/publicdomain/zero/1.0/deed.ja)
