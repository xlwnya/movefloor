﻿using UdonSharp;
using UnityEngine;

namespace Xlwnya.MoveFloor.USharp.Debugging
{
    [UdonBehaviourSyncMode(BehaviourSyncMode.NoVariableSync)]
    public class AnimatorReset : UdonSharpBehaviour
    {
        public Animator buttonSelf;
        public Animator targetAnimator;
        public string targetStateName;
        public int targetLayer = -1;
        public float resetNormalizedTime = 0f;
    
        public override void Interact()
        {
            // ボタン処理
            if (buttonSelf.enabled) return;
            buttonSelf.enabled = true;
        
            // 再生位置リセット
            targetAnimator.Play(targetStateName, targetLayer, resetNormalizedTime);
        }

        public void StopAnimator()
        {
            buttonSelf.enabled = false;
        }
    }
}
