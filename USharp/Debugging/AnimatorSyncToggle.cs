﻿using UdonSharp;
using UnityEngine;

namespace Xlwnya.MoveFloor.USharp.Debugging
{
    [UdonBehaviourSyncMode(BehaviourSyncMode.NoVariableSync)]
    public class AnimatorSyncToggle : UdonSharpBehaviour
    {
        public Animator buttonSelf;

        public LoopAnimatorSync animatorSync;
    
        private readonly int _bool = Animator.StringToHash("Bool");

        public override void Interact()
        {
            // ボタン処理
            bool value = !buttonSelf.GetBool(_bool);
            buttonSelf.SetBool(_bool, value);
            buttonSelf.enabled = true;

            animatorSync.enabled = value;
        }

        public void StopAnimator()
        {
            buttonSelf.enabled = false;
        }
    }
}
