﻿using UdonSharp;
using UnityEngine;
using UnityEngine.UI;
using VRC.SDKBase;

namespace Xlwnya.MoveFloor.USharp.Debugging
{
    [UdonBehaviourSyncMode(BehaviourSyncMode.Manual)]
    public class ChangeOwner : UdonSharpBehaviour
    {
        public Animator buttonSelf;
        public GameObject target;
        public Text ownerNameText;
        public int updateInterval = 5;

        [UdonSynced] private bool _execSetOwner = false;
        [UdonSynced] private int _playerId;
        private int _updateCount = 0;
        
        public override void Interact()
        {
            // ボタン処理
            if (buttonSelf.enabled) return;
            buttonSelf.enabled = true;

            var players = VRCPlayerApi.GetPlayers(new VRCPlayerApi[VRCPlayerApi.GetPlayerCount()]);
            var targetPlayer = players[Random.Range(0, players.Length)];
            if (Networking.LocalPlayer.playerId == targetPlayer.playerId)
            {
                _SetTargetOwner();
            }
            else
            {
                Networking.SetOwner(Networking.LocalPlayer, gameObject);
                _execSetOwner = true;
                _playerId = targetPlayer.playerId;
                RequestSerialization();
            }
        }

        public override void OnDeserialization()
        {
            bool ownerSet = false;
            if (_execSetOwner && Networking.LocalPlayer.playerId == _playerId)
            {
                ownerSet = _SetTargetOwner();
            }
            Debug.Log($"ChangeOwner.OnDeserialization: setOwner:{_execSetOwner}, playerId:{_playerId}, localPlayerId:{Networking.LocalPlayer.playerId}, ownerSet:{ownerSet}");
        }

        private bool _SetTargetOwner()
        {
            // 既にLocalPlayerがtargetのOwnerである場合SetOwnerしない
            if (Networking.IsOwner(Networking.LocalPlayer, target)) return false;
            
            Networking.SetOwner(Networking.LocalPlayer, target);
            return true;
        }

        private void Update()
        {
            if (++_updateCount < updateInterval) return;
            
            var targetOwner = Networking.GetOwner(target);
            if (targetOwner == null || !targetOwner.IsValid()) return;
            
            ownerNameText.text = targetOwner.displayName;
        }

        public void StopAnimator()
        {
            buttonSelf.enabled = false;
        }
    }
}