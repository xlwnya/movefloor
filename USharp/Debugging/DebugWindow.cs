﻿using UdonSharp;
using UnityEngine.UI;
using VRC.SDKBase;

namespace Xlwnya.MoveFloor.USharp.Debugging
{
    [UdonBehaviourSyncMode(BehaviourSyncMode.NoVariableSync)]
    public class DebugWindow : UdonSharpBehaviour
    {
        public Text text;
        public UdonSharpBehaviour[] debugTargets;
        public string[] debugTargetNames;
        public int updateInterval = 5;

        private VRCPlayerApi _localPlayer;
        private int _updateCount = 0;
        private bool _isDebugEnabled = false;
        private string[] _messageLines = null;
        
        private void Start()
        {
            _localPlayer = Networking.LocalPlayer;
        }

        public override void OnPickupUseDown()
        {
            _isDebugEnabled = !_isDebugEnabled;
            if (!_isDebugEnabled)
            {
                text.text = "Debug: OFF";
            }
        }

        private void Update()
        {
            if (!_isDebugEnabled) return;
            if (++_updateCount < updateInterval) return;
            if (_localPlayer == null || !_localPlayer.IsValid()) return;

            var velocity = _localPlayer.GetVelocity();

            if (_messageLines == null) _messageLines = new string[debugTargets.Length+3];
            int messageLineIndex = 0;
            _messageLines[messageLineIndex++] = "Debug:";
            _messageLines[messageLineIndex++] = $"Velocity:{velocity.ToString()}, {velocity.magnitude}";
            _messageLines[messageLineIndex++] = $"IsPlayerGrounded:{_localPlayer.IsPlayerGrounded()}";
            
            for (int i = 0; i < debugTargets.Length; i++)
            {
                var target = (DebugTargetPrototype)debugTargets[i];
                string targetName = null;
                if (i < debugTargetNames.Length) targetName = debugTargetNames[i];
                var debugInfo = target._DebugInfo(targetName);
                if (debugInfo != null)
                {
                    _messageLines[messageLineIndex++] = debugInfo;
                }
            }

            text.text = string.Join("\n", _messageLines, 0, messageLineIndex);
        }
    }
}