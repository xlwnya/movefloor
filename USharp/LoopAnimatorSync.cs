﻿using UdonSharp;
using UnityEngine;
using VRC.SDKBase;

namespace Xlwnya.MoveFloor.USharp
{
    [UdonBehaviourSyncMode(BehaviourSyncMode.Manual)]
    public class LoopAnimatorSync : UdonSharpBehaviour
    {
        // Animatorのループ周期(この間隔で_Sync()が呼ばれるはず)
        [Tooltip("Animator loop cycle time(sec)")]
        public float loopTime = 10f;
        
        // _Sync() called time(master)
        [UdonSynced] private double _lastSyncTime = 0;

        // _Sync() called time(local)
        private double _localLastSyncTime = 0;
        private bool _hasLocalLastSyncTime = false;
        
        // UdonSynced ready
        private bool _initialized = false;
        
        // for debug
        private float _diffCycle = 0;
        private float _speed = 0;
        
        private Animator _animator;
        
        private void Start()
        {
            _animator = GetComponent<Animator>();
        }

        public override void OnOwnershipTransferred(VRCPlayerApi player)
        {
            if (Networking.IsOwner(Networking.LocalPlayer, gameObject))
            {
                _animator.speed = 1; // Ownerになった場合Animatorの再生速度をリセットする
            }
        }

        public override void OnDeserialization()
        {
            if (!_initialized)
            {
                if (!_hasLocalLastSyncTime)
                {
                    Debug.Log($"LoopAnimatorSync.OnDeserialization !_hasLocalLastSyncTime _lastSyncTime:{_lastSyncTime} _localLastSyncTime:{_localLastSyncTime}");
                    return;
                }

                // masterから初回同期時刻データが来た場合
                var currentTime = Networking.GetServerTimeInSeconds();
                
                // 次回の同期タイミングを推定(差しか見てないのでいらないかも)
                var loopPos = (currentTime - _localLastSyncTime) / (loopTime * _animator.speed);
                var remainLoop = 1 - loopPos;
                var nextSync = currentTime + remainLoop * loopTime;

                // masterとの周期ずれ量を推定(0～1)
                var diffCycle = (float)((nextSync - _lastSyncTime) / loopTime % 1);
                if (diffCycle < 0) diffCycle += 1;
                
                // とりあえず1周期分で同期するような速度を設定。実際の同期は次の周期での処理で行う。
                var speed = diffCycle < 0.5 ? 1 / (1 - diffCycle) : 1 / (2 - diffCycle);
                _animator.speed = speed;
                
                _diffCycle = diffCycle;
                _speed = speed;
                Debug.Log($"LoopAnimatorSync OnDeserialization diffTime:{diffCycle} speed:{speed}");
            }
            else
            {
                Debug.Log($"LoopAnimatorSync OnDeserialization _initialized _lastSyncTime:{_lastSyncTime} _localLastSyncTime:{_localLastSyncTime}");
            }
            _initialized = true;
        }

        // Animatorのループ周期の開始タイミングでcallする
        public void _Sync()
        {
            _localLastSyncTime = Networking.GetServerTimeInSeconds();
            _hasLocalLastSyncTime = true;
            if (Networking.LocalPlayer == null) return;
            if (Networking.IsOwner(Networking.LocalPlayer, gameObject))
            {
                // Owner
                _lastSyncTime = _localLastSyncTime;
                RequestSerialization();
                Debug.Log($"LoopAnimatorSync._Sync Owner _lastSyncTime:{_lastSyncTime}");
                _animator.speed = 1; // Ownerの場合Animatorの再生速度を一応リセットしておく
                return;
            }

            if (!_initialized)
            {
                Debug.Log($"LoopAnimatorSync._Sync !_initialized _lastSyncTime:{_lastSyncTime} _SyncTime:{_localLastSyncTime}");
                return;
            }
        
            // not Owner && _initialized
            // masterとの周期ずれ量を推定(0～1)
            var diffCycle = (float)((_localLastSyncTime - _lastSyncTime) / loopTime % 1);
            if (diffCycle < 0) diffCycle += 1;

            // 周期を同期させるために加速速を行う。
            // localは周期の開始時点にいるはずなので、diffCycle=現時点のmasterの位置(0～1)になっている。
            // diffCycle < 0.5 の場合加速、diffCycle >= 0.5 の場合減速して周期を合わせる。
            var speed = diffCycle < 0.5 ? 1 / (1 - diffCycle) : 1 / (2 - diffCycle);
            _animator.speed = speed;
            
            _diffCycle = diffCycle;
            _speed = speed;
            Debug.Log($"LoopAnimatorSync._Sync diffTime:{diffCycle} speed:{speed} _lastSyncTime:{_lastSyncTime} _SyncTime:{_localLastSyncTime}");
        }

        public string _DebugInfo(string objectName)
        {
            if (objectName == null) objectName = name;
            
            if (Networking.LocalPlayer != null && Networking.IsOwner(Networking.LocalPlayer, gameObject))
            {
                return $"{objectName}: Owner(not sync)";
            }
            var diff = _diffCycle < 0.5 ? _diffCycle : 1 - _diffCycle;
            return $"{objectName}: speed:{_speed:F2} diff:{diff:F3}";
        }
    }
}
