﻿using UdonSharp;
using UnityEngine;
using VRC.SDKBase;
using VRC.Udon;

namespace Xlwnya.MoveFloor.USharp
{
    [UdonBehaviourSyncMode(BehaviourSyncMode.NoVariableSync)]
    public class MFloor : UdonSharpBehaviour
    {
        private UdonSharpBehaviour _playerMover;

        // PlayerMoverかどうかの判定用
        public const bool IsMFloorPlayerMover = false;

        private void Start()
        {
            // 同じGameObjectに設定されているPlayerMoverを取得
            foreach (var u in GetComponents(typeof(UdonBehaviour)))
            {
                var us = (UdonSharpBehaviour)u;
                if (us == null) continue;

                var isPlayerMover = (bool)us.GetProgramVariable(nameof(IsMFloorPlayerMover));
                if (isPlayerMover)
                {
                    _playerMover = us;
                    break;
                }
            }
            if (_playerMover == null)
            {
                Debug.LogWarning("PlayerMover not found.");
            }
        }

        public override void OnPlayerRespawn(VRCPlayerApi player)
        {
            if (!player.isLocal || _playerMover == null) return;
            _playerMover.enabled = false;
        }

        public override void OnPlayerTriggerEnter(VRCPlayerApi player)
        {
            if (!player.isLocal || _playerMover == null) return;
            _playerMover.enabled = true;
        }

        public override void OnPlayerTriggerExit(VRCPlayerApi player)
        {
            if (!player.isLocal || _playerMover == null) return;
            _playerMover.enabled = false;
        }
    }
}
