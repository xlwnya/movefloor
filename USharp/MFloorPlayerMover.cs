﻿using System;
using UdonSharp;
using UnityEngine;
using VRC.SDKBase;

namespace Xlwnya.MoveFloor.USharp
{
    [UdonBehaviourSyncMode(BehaviourSyncMode.NoVariableSync)]
    public class MFloorPlayerMover : UdonSharpBehaviour
    {
        // 移動の基準となる床(Noneの場合付いているGameObjectを使用する)
        [Tooltip("This GameObject is used if not specified.")]
        public Transform floor;

        // プレイヤーが浮いてる場合にオフにする
        public bool disableIfPlayerFloating = true;

        // TriggerStayが呼ばれなくなった場合に床をオフにするまでの時間(秒)
        public float nonStayExitSeconds = 1f;

        // PlayerMoverかどうかの判定用
        public const bool IsMFloorPlayerMover = true;

        private Vector3 _prevFloorPos;
        private VRCPlayerApi _localPlayer;
        private float _lastTriggerStayTime;

        [NonSerialized] public bool IsActive;

        private void OnEnable()
        {
            if (floor == null) floor = transform;
            _localPlayer = Networking.LocalPlayer;
            _prevFloorPos = floor.position;
            _lastTriggerStayTime = Time.time;
        }

        public override void OnPlayerTriggerStay(VRCPlayerApi player)
        {
            if (!player.isLocal) return;
            _lastTriggerStayTime = Time.time;
        }

        private void Update()
        {
            if (_localPlayer == null) return;
            if (Time.time - _lastTriggerStayTime > nonStayExitSeconds)
            {
                enabled = false;
                return;
            }

            IsActive = false;

            // 床の移動速度
            var floorPos = floor.position;
            var floorMovement = floorPos - _prevFloorPos;
            _prevFloorPos = floorPos;

            // 床が移動していない場合処理スキップ
            if (floorMovement.magnitude <= 0.0f) return;

            // プレーヤが床に接地していない場合処理スキップ
            if (disableIfPlayerFloating && !_localPlayer.IsPlayerGrounded()) return;

            // プレイヤー位置に床の動きを反映する
            var playerPos = _localPlayer.GetPosition();
            playerPos += floorMovement;
            _localPlayer.TeleportTo(playerPos, _localPlayer.GetRotation(),
                VRC_SceneDescriptor.SpawnOrientation.Default, true);

            IsActive = true;
        }
        
        public string _DebugInfo(string objectName)
        {
            if (objectName == null) objectName = name;
            return $"{objectName}: enable:{enabled}, active:{IsActive}";
        }
    }
}